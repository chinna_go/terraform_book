#Terraform code for providers and required versions
#Add your points 
terraform {
  required_version = ">= 1.0"
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "3.63.0"
    }
  }
}
provider "google" {
  credentials = file("$service_account.json")
  project = "$project_name"
  region = "$region"
  zone = "$zone"  
}
### Terraform providers code and random resource code for Azure Provisoners ####
terraform {
  required_version = ">= 1.0"
  required_providers {
    azurerm = {
      source = "hashicorp/google"
      version = "3.63.0"
    }
    random = {
      source = "hashicorp/random"
      version = "3.1.0"
    }
  }
}

#add client details
provider "azurerm" {
  features {}
  subscription_id = "add azure subscription"
  client_id = "add client id"
  client_secret = "add client secrets"
  tenant_id = "add tenant id"
}

#features can control specific properties of the Azure Key Vault
provider "azurerm" {
  version = "2.54.0"
  features {
    key_vault {
      recover_soft_deleted_secrets = false
    }
  }
}
#configure the Random Provider
provider "random" {}
resource "random_integer" "rand" {
  min = 1
  max = 50
}


### adding AWS providers information ###
terraform {
  required_version = ">=1.0"
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 3.35.5"
    }
  }
}


provider "aws" {
  region = "$region"
  access_key = "$access_key" #provide in secure way
  secret_key = "$secret_key" # provide in secure way
}

